from flask import Flask, render_template
from flask_socketio import SocketIO
import numpy as np
import random, string

app = Flask(__name__)
sio = SocketIO(app, async_mode='eventlet')

# WindowSize = list()

Apple = list()
Players = {}

@app.route('/')
def hello_world():
    return render_template('index.html', title="Здорово!")

# создаю яблоко
@sio.on('createAppleOrUpdate', namespace='/flask')
def createAppleOrUpdate(innerWidth, innerHeight):
    global Apple
    if not Apple:
        width = int((random.randint(0, innerWidth) / 30)) * 30
        height = int((random.randint(0, innerHeight) / 30)) * 30 
        Apple.extend([width, height])
        sio.emit('returnAppleCoordinates', Apple, namespace='/flask')
    else:
        sio.emit('returnAppleCoordinates', 'false', namespace='/flask')

# создаю игрока
@sio.on('createPlayer', namespace='/flask')
def createPlayer(innerWidth, innerHeight):
    global Players
    if not Players:
        name = '0'
    else:
        name = str(len(Players)) 
    width = int((random.randint(0, innerWidth) / 30)) * 30
    height = int((random.randint(0, innerHeight) / 30)) * 30 
    Players[name] = dict({
        "width": width,
        "height": height,
        "size": 1
    })
    sio.emit('returnPlayers', Players, namespace='/flask')

# меняю расположение змей
@sio.on('changeMove', namespace='/flask')
def changeMove(move, player, innerWidth, innerHeight):
    global Players
    global Apple
    if move == 1:
        Players[player]['width'] =  Players[player]['width']+30
    elif move == 2:
        Players[player]['height'] =  Players[player]['height']+30
    elif move == 3:
        Players[player]['width'] =  Players[player]['width']-30
    elif move == 4:
        Players[player]['height'] =  Players[player]['height']-30

    if Players[player]['width'] >= innerWidth:
        Players[player]['width'] = 0
    elif Players[player]['height'] >= innerHeight:
        Players[player]['height'] = 0
    elif Players[player]['width'] <= 0:
        Players[player]['width'] = innerWidth
    elif Players[player]['height'] <= 0:
        Players[player]['height'] = innerHeight
    if Apple:
        if Players[player]['height'] == Apple[1] and Players[player]['width'] == Apple[0]:
            # Players[player]["size"] += 1
            Apple.clear()
            sio.emit('returnChangeMove',  Players, namespace='/flask')
    sio.emit('returnChangeMove', Players, namespace='/flask')

if __name__ == '__main__':
    sio.run(app, debug=True)
    # app.run()  